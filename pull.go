package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/fatih/color"
	cli "github.com/jawher/mow.cli"
)

// Pull method runs the pull sub command of the application.
// This pulls the artifacts associated with a given build ID.
// '--print' option just prints the URL of the artifacts and exits.
func (a *App) Pull(cmd *cli.Cmd) {
	bid := cmd.StringArg("BUILDID", "", "Build ID of the built package to be pulled from the server")
	print := cmd.BoolOpt("print", false, "Option to just print the URLs of the packages instead of downloading them")
	cmd.Action = func() {
		err := a.ReadConfig()
		if err != nil {
			if os.IsNotExist(err) {
				a.printError("reading the configuration", "authentication not done. Run the login command to connect to an account")
				return
			}
			a.printError("reading the configuration", err.Error())
			return
		}

		status, err := a.GetStatus("status", *bid)
		if err != nil {
			a.printError("getting the status", err.Error())
			return
		}

		if !status.Completed {
			fmt.Println(color.YellowString("Bob is busy building... 😉"))
			return
		}

		if !status.Success {
			a.printError("fetching the builds", "cannot pull failed builds")
			return
		}
		a.fetchPackages(status, *print)
	}
}

func (a *App) fetchPackages(status *Status, print bool) {
	for _, pkg := range status.Packages {
		pkgname := pkg.Pkgname + "-" + pkg.Pkgver + "-" + pkg.Arch + ".pkg.tar.xz"
		pkgurl := a.GetEndpointURL(path.Join("repo", status.User, pkgname))
		if print {
			fmt.Println(pkgurl)
			continue
		}
		resp, err := http.Get(pkgurl)
		if err != nil {
			a.printError("pulling the package", err.Error()+". Skipping.")
			continue
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			a.printError("pulling the package", fmt.Sprintf("got %d http status code. Skipping", resp.StatusCode))
			continue
		}

		outfile, err := os.Create(pkgname)
		if err != nil {
			a.printError("pulling the package", err.Error()+". Skipping.")
			continue
		}

		progressBar := []byte{'-', '\\', '|', '/'}

		start := time.Now()
		done := int64(0)
		pbnum := 0
		for {
			p := make([]byte, 4096)
			fmt.Printf("\r[%s] Pulling %s\t%03d %% at %.2f MB/s",
				color.YellowString(string(progressBar[pbnum%4])),
				pkgname,
				int64(float64(done)*100/float64(resp.ContentLength)),
				float64(done)/1024.0/1024.0/time.Now().Sub(start).Seconds(),
			)
			rd, err := resp.Body.Read(p)
			if err != nil {
				if err == io.EOF {
					_, err = outfile.Write(p[:rd])
					if err != nil {
						println()
						a.printError("pulling the package", err.Error()+". Skipping")
						break
					}
					done += int64(rd)
					fmt.Printf("\r[%s] Pulling %s\t%03d %% at %.2f MB/s\n",
						color.GreenString("✔"),
						pkgname,
						int64(float64(done)*100/float64(resp.ContentLength)),
						float64(done)/1024.0/1024.0/time.Now().Sub(start).Seconds(),
					)
					break
				}
				println()
				a.printError("pulling the package", err.Error()+". Skipping")
				break
			}

			_, err = outfile.Write(p[:rd])
			if err != nil {
				println()
				a.printError("pulling the package", err.Error()+". Skipping")
				break
			}
			done += int64(rd)
			pbnum++
		}
	}
}
