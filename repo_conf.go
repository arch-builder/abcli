package main

import (
	"fmt"
	"os"
	"path"

	cli "github.com/jawher/mow.cli"
)

// Repoconf method runs the repoconf sub command of the application.
// This prints the repo configuration for the user that can be added to the /etc/pacman.conf
func (a *App) Repoconf(cmd *cli.Cmd) {
	cmd.Action = func() {
		err := a.ReadConfig()
		if err != nil {
			if os.IsNotExist(err) {
				a.printError("reading the configuration", "authentication not done. Run the login command to connect to an account")
				return
			}
			a.printError("reading the configuration", err.Error())
			return
		}

		username, err := a.GetUsername()
		if err != nil {
			a.printError("getting the user configuration", err.Error())
			return
		}

		fmt.Printf("[%s]\nServer = %s\nSigLevel = Never\n", username, a.GetEndpointURL(path.Join("repo", "$repo")))
	}
}
