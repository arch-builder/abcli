package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	"github.com/fatih/color"
	cli "github.com/jawher/mow.cli"
)

// List method runs the list sub command of the application
// This prints the list of all the builds triggered by the user and
// colourizes them according to the status of the said build.
// (Green : Success, Yellow : running, Red : Failed)
func (a *App) List(cmd *cli.Cmd) {
	cmd.Action = func() {
		err := a.ReadConfig()
		if err != nil {
			if os.IsNotExist(err) {
				a.printError("reading the configuration", "authentication not done. Run the login command to connect to an account")
				return
			}
			a.printError("reading the configuration", err.Error())
			return
		}
		bodybs, _ := json.Marshal(&Args{
			APIToken: a.Token,
			Args:     []string{},
		})

		body := bytes.NewBuffer(bodybs)

		msg := Message{}

		req, _ := http.NewRequest("POST", a.GetEndpointURL("list"), body)
		req.Header.Set("Content-Type", "application/json")
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			a.printError("making the API request", err.Error())
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			if resp.StatusCode == 407 {
				a.printError("receiving the response", "authentication not done. Run the login command to connect to an account")
				return
			}
			msg.Reply = ""
			err := json.NewDecoder(resp.Body).Decode(&msg)
			if err != nil {
				a.printError("decoding the response", err.Error())
				return
			}

			a.printError("listing builds", msg.Reply.(string))
			return
		}

		msg.Reply = &ListReply{}

		err = json.NewDecoder(resp.Body).Decode(&msg)
		if err != nil {
			a.printError("decoding the response", err.Error())
			return
		}

		lr := msg.Reply.(*ListReply)

		if lr.IsError {
			a.printError("listing builds", lr.Message)
			return
		}
		fmt.Println("List of all builds triggered :")
		for _, bid := range lr.List {
			stat, err := a.GetStatus("status", bid)
			if err == nil {
				a.printStatus(stat)
			}
		}
	}
}

func (a *App) printStatus(stat *Status) {
	if !stat.Completed && !stat.Success {
		fmt.Printf("%s\t: %s\n", color.YellowString(" %s %s", "⌛", stat.BuildID), filepath.Base(stat.URL.Path))
		return
	}

	if stat.Completed && stat.Success {
		fmt.Printf("%s\t: %s\n", color.GreenString(" %s %s", "✔", stat.BuildID), filepath.Base(stat.URL.Path))
		return
	}
	fmt.Printf("%s\t: %s\n", color.RedString(" %s %s", "✘", stat.BuildID), filepath.Base(stat.URL.Path))
}
