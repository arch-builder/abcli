package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/fatih/color"
	cli "github.com/jawher/mow.cli"
)

// Status mehtod runs the status sub-command of the application.
// Status prints the status of the said build.
func (a *App) Status(cmd *cli.Cmd) {
	buildid := cmd.StringArg("BUILDID", "", "Build ID specified by the server when triggered using the build command")
	cmd.Action = func() {
		err := a.ReadConfig()
		if err != nil {
			if os.IsNotExist(err) {
				a.printError("reading the configuration", "authentication not done. Run the login command to connect to an account")
				return
			}
			a.printError("reading the configuration", err.Error())
			return
		}

		status, err := a.GetStatus("status", *buildid)
		if err != nil {
			a.printError("getting the status", err.Error())
			return
		}

		start := time.Unix(status.Start, 0)
		end := time.Unix(status.End, 0)
		if !status.Completed {
			fmt.Printf("%s Build with build id %s started at %s\n",
				color.YellowString("%s", "🔨"), color.YellowString("%s", status.BuildID),
				color.YellowString("%s", start),
			)
			fmt.Printf("%s Time elapsed : %s\n",
				color.YellowString("%s", "⌛"),
				time.Now().Sub(start),
			)
			fmt.Printf("%s URL : %s\n",
				color.YellowString("%s", "🌐"),
				status.URL,
			)
			if status.Message != "" {
				fmt.Printf("%s Message : %s\n",
					color.YellowString("%s", "🖅"),
					color.YellowString("%s", status.Message),
				)
			}
			return
		}

		if !status.Success {
			fmt.Printf("%s Build with build id %s started at %s and failed\n",
				color.RedString("%s", "✘"), color.RedString("%s", status.BuildID),
				color.RedString("%s", start),
			)
			fmt.Printf("%s Time elapsed : %s\n",
				color.RedString("%s", "⌛"),
				color.RedString("%s", end.Sub(start)),
			)
			fmt.Printf("%s URL : %s\n",
				color.RedString("%s", "🌐"),
				color.RedString("%s", status.URL),
			)

			if status.Message != "" {
				fmt.Printf("%s Message : %s\n",
					color.RedString("%s", "🖅"),
					color.RedString("%s", status.Message),
				)
			}
			return
		}
		fmt.Printf("%s Build with build id %s started at %s and successfully passed.\n",
			color.GreenString("%s", "✔"), color.GreenString("%s", status.BuildID),
			color.GreenString("%s", start),
		)
		fmt.Printf("%s Time elapsed : %s\n",
			color.GreenString("%s", "⌛"),
			color.GreenString("%s", end.Sub(start)),
		)
		fmt.Printf("%s URL : %s\n",
			color.GreenString("%s", "🌐"),
			color.GreenString("%s", status.URL),
		)
	}
}

// GetStatus method is used to get the status of a build.
// Generally Status object is sent by 'build' and 'status' methods.
// So a build api call can be called by changing the endpoint and the arguments.
func (a *App) GetStatus(endpoint, arg string) (*Status, error) {
	bodybs, _ := json.Marshal(&Args{
		APIToken: a.Token,
		Args: []string{
			arg,
		},
	})

	body := bytes.NewBuffer(bodybs)

	msg := Message{}

	req, _ := http.NewRequest("POST", a.GetEndpointURL(endpoint), body)
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		if resp.StatusCode == 407 {
			return nil, errors.New("not authenticated or the api token expired")
		}
		msg.Reply = ""
		err := json.NewDecoder(resp.Body).Decode(&msg)
		if err != nil {
			return nil, err
		}

		return nil, errors.New(msg.Reply.(string))
	}

	msg.Reply = &Status{}
	err = json.NewDecoder(resp.Body).Decode(&msg)
	if err != nil {
		return nil, err
	}

	status := msg.Reply.(*Status)

	return status, nil
}
