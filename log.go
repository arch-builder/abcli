package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	cli "github.com/jawher/mow.cli"
)

// Log method runs the function for the log sub command.
// This prints the log of the said build. 'stdout' of the build is
// printed by default unless --stderr is passed, which prints the stderr.
// The logs can be tracked by using the --live command.
func (a *App) Log(cmd *cli.Cmd) {
	bid := cmd.StringArg("BUILDID", "", "Build ID of a build to get the log for")
	out := cmd.BoolOpt("stderr", false, "stderr option is used to print the stderr instead stdout")
	live := cmd.BoolOpt("live", false, "track the log of the build")
	cmd.Action = func() {
		err := a.ReadConfig()
		if err != nil {
			if os.IsNotExist(err) {
				a.printError("reading the configuration", "authentication not done. Run the login command to connect to an account")
				return
			}
			a.printError("reading the configuration", err.Error())
			return
		}

		llog, err := a.GetLog(*bid)
		if err != nil {
			a.printError("getting the logs", err.Error())
			return
		}

		var bs []byte

		if *out {
			bs, err = base64.StdEncoding.DecodeString(llog.Stderr)
			if err != nil {
				a.printError("decoding the base64 logs", err.Error())
				return
			}
		} else {
			bs, err = base64.StdEncoding.DecodeString(llog.Stdout)
			if err != nil {
				a.printError("decoding the base64 logs", err.Error())
				return
			}
		}

		lines := string(bs)
		fmt.Print(lines)

		if *live {
			prev := string(bs)

			for {
				llog, err := a.GetLog(*bid)
				if err != nil {
					a.printError("getting the logs", err.Error())
					break
				}

				var bs []byte

				if *out {
					bs, err = base64.StdEncoding.DecodeString(llog.Stderr)
					if err != nil {
						a.printError("decoding the base64 logs", err.Error())
						break
					}
				} else {
					bs, err = base64.StdEncoding.DecodeString(llog.Stdout)
					if err != nil {
						a.printError("decoding the base64 logs", err.Error())
						break
					}
				}

				lines := strings.Replace(string(bs), prev, "", 1)
				fmt.Print(lines)
				prev = string(bs)
				time.Sleep(200 * time.Millisecond)
			}
		}

	}
}

// GetLog method is used to get the log of a build with 'arg' build id.
func (a *App) GetLog(arg string) (*LiveLog, error) {
	bodybs, _ := json.Marshal(&Args{
		APIToken: a.Token,
		Args: []string{
			arg,
		},
	})

	body := bytes.NewBuffer(bodybs)

	msg := Message{}

	req, _ := http.NewRequest("POST", a.GetEndpointURL("log"), body)
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		if resp.StatusCode == 407 {
			return nil, errors.New("not authenticated or the api token expired")
		}
		msg.Reply = ""
		err := json.NewDecoder(resp.Body).Decode(&msg)
		if err != nil {
			return nil, err
		}

		return nil, errors.New(msg.Reply.(string))
	}

	msg.Reply = &LiveLog{}
	err = json.NewDecoder(resp.Body).Decode(&msg)
	if err != nil {
		return nil, err
	}

	llog := msg.Reply.(*LiveLog)

	return llog, nil
}
