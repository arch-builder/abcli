package main

import (
	"fmt"
	"os"
	"time"

	"github.com/fatih/color"
	cli "github.com/jawher/mow.cli"
)

// Build method runs the build sub command of the application
// This instructs the remote API server to build the PKGBUILD from a git repo.
func (a *App) Build(cmd *cli.Cmd) {
	uri := cmd.StringArg("URL", "", "URL of the git repo containing the PKGBUILD")
	cmd.Action = func() {
		err := a.ReadConfig()
		if err != nil {
			if os.IsNotExist(err) {
				a.printError("reading the configuration", "authentication not done. Run the login command to connect to an account")
				return
			}
			a.printError("reading the configuration", err.Error())
			return
		}

		status, err := a.GetStatus("build", *uri)
		if err != nil {
			a.printError("getting the status", err.Error())
			return
		}

		fmt.Printf("%s Build with build id %s started at %s\n",
			color.YellowString("%s", "⌛"), color.YellowString("%s", status.BuildID),
			time.Unix(status.Start, 0),
		)
	}
}
