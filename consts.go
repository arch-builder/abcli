package main

// Reply type constants
const (
	ReplyFailure  int64 = -1
	ReplySuccess        = 0
	ReplyRedirect       = 1
	ReplyAPIToken       = 2
	ReplyStatus         = 3
	ReplyList           = 4
	ReplyLog            = 5
	ReplyWait           = 6
	ReplyUser           = 7
)
