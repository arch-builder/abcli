package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path/filepath"

	"github.com/fatih/color"
	cli "github.com/jawher/mow.cli"
)

// RedirectStruct contains fields to read the login data sent
// by the server during the login reguest
type RedirectStruct struct {
	URL    string `json:"url"`
	Secret string `json:"secret"`
}

// Login method runs the login sub command of the application.
// This starts the gitlab OAuth authentication flow to authenticate the users.
func (a *App) Login(cmd *cli.Cmd) {
	cmd.Action = func() {
		rs := RedirectStruct{}
		msg := Message{
			Reply: &rs,
		}

		uri := ""

		fmt.Print("Welcome to abcli, Enter the Base URL of the Arch Builder Service: ")
		fmt.Scanf("%s", &uri)

		purl, err := url.Parse(uri)
		if err != nil {
			a.printError("parsing the URL passed", err.Error())
			return
		}

		a.NodeURL = purl

		resp, err := http.Get(a.GetEndpointURL("login"))
		if err != nil {
			a.printError("getting user credentials", err.Error())
			return
		}
		defer resp.Body.Close()

		err = json.NewDecoder(resp.Body).Decode(&msg)
		if err != nil {
			a.printError("getting user credentials", err.Error())
			return
		}
		if msg.ReplyType != ReplyRedirect {
			a.printError("getting user credentials", msg.Reply.(string))
			return
		}

		err = openLinkInBrowser(rs.URL)
		if err != nil {
			a.printError("getting user credentials", "expected a redirect URL as a string")
			return
		}

		apiToken := ""

		fmt.Printf("%sPolling the server for an api token....\n", color.YellowString("%s", "⌛"))

		for {
			apimsg := Message{
				Reply: "",
			}
			resp, err := http.Get(a.GetEndpointURL("getToken") + "?secret=" + rs.Secret)
			if err != nil {
				a.printError("getting the api token", err.Error())
				fmt.Println("Try accessing the URL \"" + a.GetEndpointURL("getToken") + "?secret=" + rs.Secret + "\" to get the api token.")
				fmt.Printf("Once you get the token, save it in \"%s\".\n", filepath.Join(os.Getenv("HOME"), ".config", "abcli.conf"))
				return
			}
			defer resp.Body.Close()
			err = json.NewDecoder(resp.Body).Decode(&apimsg)
			if err != nil {
				a.printError("getting the api token", err.Error())
				return
			}

			if resp.StatusCode != 200 || apimsg.ReplyType == ReplyFailure {
				a.printError("getting the api token", msg.Reply.(string))
				return
			}

			if apimsg.ReplyType == ReplyWait {
				continue
			}

			if apimsg.ReplyType == ReplyAPIToken {
				apiToken = apimsg.Reply.(string)
				break
			}

		}

		a.Token = apiToken

		err = a.WriteConfig()
		if err != nil {
			a.printError("getting user credentials", "expected a redirect URL as a string")
			return
		}

		fmt.Printf("%s Successfully authenticated!!\n", color.GreenString("%s", "✔"))
	}
}
