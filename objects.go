package main

import "net/url"

// Message struct is the generic struct to decode the JSON data sent
// as a reply by the arch builder API server
type Message struct {
	ReplyType   int64       `json:"reply_type"`
	ReplyString string      `json:"reply_string"`
	Reply       interface{} `json:"reply"`
}

// Args struct is the generic struct to encode the JSON data and sent as
// request ti the arch builder API server.
type Args struct {
	APIToken string   `json:"api_token"`
	Args     []string `json:"args"`
}

// ListReply struct contains all the fields to represent the data
// sent by the server during a list request
type ListReply struct {
	List    []string `json:"list"`
	Message string   `json:"message"`
	IsError bool     `json:"is_error"`
}

// PkgInfo struct contains all the fields to represent the
// package information of a build package
type PkgInfo struct {
	Pkgname   string `json:"pkgname,omitempty"`
	Pkgver    string `json:"pkgver,omitempty"`
	Pkgdesc   string `json:"pkgdesc,omitempty"`
	URL       string `json:"pkgurl,omitempty"`
	BuildDate int64  `json:"pkgdate,omitempty"`
	Packager  string `json:"pkger,omitempty"`
	Size      int64  `json:"size,omitempty"`
	Arch      string `json:"arch,omitempty"`
	License   string `json:"license,omitempty"`
}

// Status struct contains all the fields to represent the data
// sent by the server during a status request
type Status struct {
	User      string     `json:"user"`
	BuildID   string     `json:"build_id"`
	URL       *url.URL   `json:"url"`
	Start     int64      `json:"start_timestamp"`
	End       int64      `json:"end_timestamp"`
	Completed bool       `json:"completed"`
	Success   bool       `json:"success"`
	Message   string     `json:"message"`
	Packages  []*PkgInfo `json:"packages,omitempty"`
}

// LiveLog struct contains all the fields to represent the data
// sent by the server during a log request
type LiveLog struct {
	User    string `json:"user"`
	BuildID string `json:"build_id"`
	Stderr  string `json:"stderr"`
	Stdout  string `json:"stdout"`
}
