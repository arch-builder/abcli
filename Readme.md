# abcli

abcli is a command line application to interact with the abapi http API service and issue commands to the binner service.

## Usage

#### login
```
abcli login
```
It'll prompt for the URL of the abapi service. After that just login and allow access to the application.
abcli will take care of everything.

#### build
```
abcli build GIT_REPO_URL
```
The binner will start building the arch linux package for the PKGBUILD in the repo.
And prints a nice status.

#### status
```
abcli status BUILD_ID
```
Prints the status of a build with a particular build ID.

#### list
```
abcli list
```
Prints the list of all the builds triggered by the user.

#### log
```
abcli log BUILD_ID
```
Prints the log of the build with the BUILD_ID.
`--stderr` prints the log printed in the Stderr of the build process.
`--live` shows the live log of the build process.

#### repoconf
```
abcli repoconf
```
Prints the repo configuration that had to be added to the `/etc/pacman.conf`
in order to install packages using pacman.

#### pull
```
abcli pull BUILD_ID
```
Downloads all the packages associated with the Build (Build ID)

The following screencast will give a rough idea of the workflow:
[![asciicast](https://asciinema.org/a/185739.png)](https://asciinema.org/a/185739)
