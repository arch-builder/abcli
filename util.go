package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/fatih/color"
)

// App struct contains fields and functions to communicate with the
// remote API server
type App struct {
	NodeURL *url.URL
	Token   string
}

var errclr = color.New(color.FgRed, color.Bold)

// printError is used to print error strings with colors.
func (a *App) printError(operation, errorString string) {
	fmt.Printf("Error occurred while %s : %s\n", operation, errclr.Sprint(errorString))
}

// openLinkInBrowser is used to open a given URL in a browser.
func openLinkInBrowser(url string) error {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	return err
}

// ReadConfig function is used to read the configuration from a
// config file located at $HOME/.config/abcli.conf
func (a *App) ReadConfig() error {
	contents, err := ioutil.ReadFile(filepath.Join(os.Getenv("HOME"), ".config", "abcli.conf"))
	if err != nil {
		return err
	}

	lines := strings.Split(string(contents), "\n")
	if len(lines) < 2 {
		return errors.New("malformed config file : not enough info to parse")
	}

	uri := ""

	for _, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}

		keys := strings.SplitN(line, "=", 2)
		if len(keys) != 2 {
			return errors.New("malformed config file : invalid 'key = value' pair")
		}

		switch strings.TrimSpace(keys[0]) {
		case "api_token":
			a.Token = strings.TrimSpace(keys[1])
		case "node_url":
			uri = strings.TrimSpace(keys[1])
		}
	}

	if a.Token == "" {
		return errors.New("empty token cannot be used")
	}

	a.NodeURL, err = url.Parse(uri)
	if err != nil {
		return errors.New("malformed url detected in the config file : " + err.Error())
	}

	return nil
}

// WriteConfig method writes configuration to a file (at $HOME/.config/abcli.conf)
func (a *App) WriteConfig() error {
	contents := "api_token = " + a.Token + "\nnode_url = " + a.NodeURL.String()
	return ioutil.WriteFile(filepath.Join(os.Getenv("HOME"), ".config", "abcli.conf"), []byte(contents), 0666)
}

// GetEndpointURL is used to get the full URL of a given endpoint from the Base URL
// provided in the configuration.
func (a *App) GetEndpointURL(ep string) string {
	x := a.NodeURL.Path
	a.NodeURL.Path = path.Join(x, ep)
	res := a.NodeURL.String()
	a.NodeURL.Path = x
	return res
}

// GetUsername is used to get the username of the authorized user.
func (a *App) GetUsername() (string, error) {
	bs := []byte{}
	br := bytes.NewBuffer(bs)

	json.NewEncoder(br).Encode(&Args{APIToken: a.Token, Args: []string{}})

	req, err := http.NewRequest("POST", a.GetEndpointURL("user"), br)
	if err != nil {
		return "", err
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	msg := Message{}
	msg.Reply = ""

	err = json.NewDecoder(resp.Body).Decode(&msg)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 || msg.ReplyType != ReplyUser {
		return "", errors.New(msg.Reply.(string))
	}

	return msg.Reply.(string), nil
}
