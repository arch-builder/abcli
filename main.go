package main

import (
	"os"

	cli "github.com/jawher/mow.cli"
)

func main() {
	cliapp := cli.App("abcli", "Command line application to interact with the Arch Builder service")
	app := &App{}
	cliapp.Command(
		"login",
		"Login command starts the login authorization process",
		app.Login,
	)
	cliapp.Command(
		"build",
		"Build command triggers a build in the Builder service for a given PKGBUILD repo",
		app.Build,
	)
	cliapp.Command(
		"status",
		"Status command is used to print the status of the given build ID",
		app.Status,
	)
	cliapp.Command(
		"list",
		"List command is used to print all the builds triggered by the user",
		app.List,
	)
	cliapp.Command(
		"log",
		"Log command is used to get the log of a given build",
		app.Log,
	)
	cliapp.Command(
		"repoconf",
		"Repoconf command is used to print the configuraion for the binary repop of the user to be added in pacman.conf",
		app.Repoconf,
	)

	cliapp.Command(
		"pull",
		"Pull command is used to pull the built artifact from the Arch Builder server",
		app.Pull,
	)

	cliapp.Run(os.Args)
}
